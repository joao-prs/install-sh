# Install Sh
Guia de instalação para usuarios de manjaro kkkkkkk 


### obrigatorio
```sh
sudo pacman -Sy
# e
sudo pacman -Su
```

### install yay
```sh
pacman -S --needed git base-devel
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```

### install via yay
basic installation
```sh
yay -Syyu
yay -S ncdu
yay -S ttf-symbola # for correction fonts
yay -S noto-fonts-emoji # for emojis
yay -R gnu-free-fonts # delete default fonts symbols
yay -S visual-studio-code-bin
```